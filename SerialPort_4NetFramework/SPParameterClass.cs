﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SerialPort_4NetFramework
{
    /// <summary>
    /// 串口参数类
    /// </summary>
    /// <typeparam name="T">value选择需要使用的值</typeparam>
    public class SPParameterClass<T>
    {
        /// <summary>
        /// 显示值
        /// </summary>
        string name;

        /// <summary>
        /// 显示值
        /// </summary>
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        /// <summary>
        /// 值
        /// </summary>
        T value;

        /// <summary>
        /// 值
        /// </summary>
        public T Value
        {
            get { return this.value; }
            set { this.value = value; }
        }

    }

    /// <summary>
    /// 串口波特率列表。
    /// 75,110,150,300,600,1200,2400,4800,9600,14400,19200,28800,38400,56000,57600,
    /// 115200,128000,230400,256000
    /// </summary>
    public enum SerialPortBaudRates
    {
        /// <summary>
        /// 波特率:75
        /// </summary>
        BaudRate_75 = 75,

        /// <summary>
        /// 波特率:110
        /// </summary>
        BaudRate_110 = 110,

        /// <summary>
        /// 波特率:150
        /// </summary>
        BaudRate_150 = 150,

        /// <summary>
        /// 波特率:300
        /// </summary>
        BaudRate_300 = 300,

        /// <summary>
        /// 波特率:600
        /// </summary>
        BaudRate_600 = 600,

        /// <summary>
        /// 波特率:1200
        /// </summary>
        BaudRate_1200 = 1200,

        /// <summary>
        /// 波特率:2400
        /// </summary>
        BaudRate_2400 = 2400,

        /// <summary>
        /// 波特率:4800
        /// </summary>
        BaudRate_4800 = 4800,

        /// <summary>
        /// 波特率:9600
        /// </summary>
        BaudRate_9600 = 9600,

        /// <summary>
        /// 波特率:14400
        /// </summary>
        BaudRate_14400 = 14400,

        /// <summary>
        /// 波特率:19200
        /// </summary>
        BaudRate_19200 = 19200,

        /// <summary>
        /// 波特率:28800
        /// </summary>
        BaudRate_28800 = 28800,

        /// <summary>
        /// 波特率:38400
        /// </summary>
        BaudRate_38400 = 38400,

        /// <summary>
        /// 波特率:56000
        /// </summary>
        BaudRate_56000 = 56000,

        /// <summary>
        /// 波特率:57600
        /// </summary>
        BaudRate_57600 = 57600,

        /// <summary>
        /// 波特率:115200
        /// </summary>
        BaudRate_115200 = 115200,

        /// <summary>
        /// 波特率:128000
        /// </summary>
        BaudRate_128000 = 128000,

        /// <summary>
        /// 波特率:230400
        /// </summary>
        BaudRate_230400 = 230400,

        /// <summary>
        /// 波特率:256000
        /// </summary>
        BaudRate_256000 = 256000
    }
}
